import java.io.*;

public class Main {

    public static void main(String[] args){

        try {
            load();
        } catch (FileNotFoundException e){
            PersonalData.F = true;
            Display.createDisplay();
            return;
        } catch (IOException a) {
        } catch (ClassNotFoundException a){
        }
        PersonalData.F = false;
        Display.createDisplay();
    }

    public static void save() {
        try{
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("temp.out"));
            out.writeObject(new PersonalData());
            out.flush();
            out.close();
        } catch (Exception e){

        }

    }

    public static void load() throws IOException, ClassNotFoundException{
        ObjectInputStream oin = new ObjectInputStream(new FileInputStream("temp.out"));
        PersonalData PD = (PersonalData) oin.readObject();
        PersonalData.Name = PD.persName;
        PersonalData.F = PD.First;
    }

}
