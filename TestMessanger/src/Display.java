import javax.naming.Context;
import javax.swing.*;
import javax.swing.text.DefaultCaret;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.BindException;

public abstract class Display {
    private static JTextArea inPutText;
    private static JTextField outPutText;
    private static JFrame window;
    private static Dimension size;
    private static Server server;
    private static Client client;

    public static void createDisplay(){
        if (PersonalData.F) Init("Ваше имя?"); else {
            JOptionPane.showMessageDialog(null,"Здравствуйте "+PersonalData.Name);}

        initComponents();

        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setLocationRelativeTo(null);
        window.setSize(size);
        mainWindow();

        window.setVisible(true);
    }

    private static void Init(String mes){
        String result=JOptionPane.showInputDialog(mes);

        if ((result.length()<3)||(result.length()>21)){
            if (result.length()<3){
                Init("Имя слишком короткое");
            } else {
                Init("Имя слишком длинное");
            }
            return;
        }

        PersonalData.Name = result;
        PersonalData.F = false;
        Main.save();
    }

    private static void initComponents(){
        window = new JFrame("Окно");
        size = new Dimension(500,500);
        inPutText = new JTextArea();
        inPutText.setLineWrap(true);
        inPutText.setWrapStyleWord(true);
        inPutText.setEditable(false);

        DefaultCaret caret = (DefaultCaret) inPutText.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

        outPutText = new JTextField();

        server = new Server();
        client = new Client();
    }

    public static void messageWindow(){
        window.getContentPane().removeAll();

        final JPanel mainPanel = new JPanel();
        final JPanel downPanel = new JPanel();
        final JButton btnSend = new JButton("Отправить");
        final JButton btnBack = new JButton("Назад");

        outPutText.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                switch (e.getKeyCode()){
                    case (KeyEvent.VK_ENTER): Click();
                }

            }
        });
        btnBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Setting.clean();
                window.setTitle("Окно");
                mainWindow();
                client.stop();
                if (Server.getRun())server.stop();
                inPutText.setText("");
            }
        });

        btnSend.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Click();
                inPutText.requestFocus();
            }
        });

        mainPanel.setLayout(new BorderLayout());
        downPanel.setLayout(new BorderLayout());

        downPanel.add(outPutText,BorderLayout.CENTER);
        downPanel.add(btnSend,BorderLayout.EAST);
        downPanel.add(btnBack,BorderLayout.WEST);

        mainPanel.add(new JScrollPane(inPutText), BorderLayout.CENTER);
        mainPanel.add(downPanel, BorderLayout.SOUTH);

        window.getContentPane().add(mainPanel);
        window.revalidate();
        window.repaint();
    }

    public static void mainWindow(){
        window.getContentPane().removeAll();

        final JPanel mainPanel = new JPanel();
        final JPanel secondPanel = new JPanel();

        final JButton btnJoin = new JButton("Подключиться");
        final JButton btnCreate = new JButton("Создать");

        btnJoin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String result = JOptionPane.showInputDialog("Введите адрес по которому хотите подключиться.");
                if (result.equals("")) return;

                String[] sock = result.split(":");
                if (sock.length != 2) {
                    JOptionPane.showMessageDialog(null,"Неправильно введены данные");
                    return;
                }

                Setting.IP4 = sock[0];
                Setting.PORT = Integer.parseInt(sock[1]);
                window.setTitle("Клиент");
                messageWindow();
                inPutText.setText("");
                client.start();
            }
        });

        btnCreate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String result = JOptionPane.showInputDialog("Введите желаемый порт.");
                if (result.equals("")) return;

                try {
                    Server.Bind(Integer.parseInt(result));
                } catch (BindException error){
                    JOptionPane.showMessageDialog(null,"Этот порт уже занят");
                    return;
                } catch (IOException error){
                    JOptionPane.showMessageDialog(null,"Неправильный порт");
                    return;
                }

                Setting.PORT=Integer.parseInt(result);
                Setting.IP4 = "127.0.0.1";

                window.setTitle("Сервер с портом " + Setting.PORT);
                messageWindow();
                inPutText.setText("");
                server.start();
                client.start();
            }
        });

        mainPanel.setLayout(new BorderLayout());
        secondPanel.setLayout(new FlowLayout());

        secondPanel.add(btnJoin);
        secondPanel.add(btnCreate);

        mainPanel.add(secondPanel, BorderLayout.CENTER);

        window.getContentPane().add(mainPanel);

        window.revalidate();
        window.repaint();
    }

    public static void setMessage(String newLine){
        String history = inPutText.getText();
        history+="\n"+newLine;
        inPutText.setText(history);
    }

    public static void Click(){
        String message;

        try {
            message = outPutText.getText();
            if (message.equals("")) return;
        } catch (Exception error){
            return;
        }

        Client.setData(PersonalData.Name+": "+message);

        outPutText.setText("");
    }
}
