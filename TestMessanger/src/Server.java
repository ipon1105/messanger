import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server implements Runnable {
    static private Socket connection;
    static private ServerSocket server;
    static private Boolean run = false;
    static private Thread thread;
    static private ArrayList<ClientHandle> clients = new ArrayList<ClientHandle>();

    public static Boolean getRun() {
        return run;
    }

    public void start(){
        run = true;
        thread = new Thread(this);
        thread.start();
    }

    public void stop(){
        run = false;

        try {
            server.close();
            for (ClientHandle h : clients){
                h.close();
            }
        } catch (IOException e){
            try {
                thread.join(1);
            }catch (InterruptedException ie){

            }
        }
    }

    @Override
    public void run() {
        while (run) {
            try {
                server = new ServerSocket(Setting.PORT, Setting.BACKLOG);

                while (run) {

                    connection = server.accept();

                    ClientHandle client = new  ClientHandle(connection,this);
                    clients.add(client);

                    new Thread(client).start();

                }

            }catch (Exception e){
                System.out.println("ERROR");
            }
            finally {
                try {
                    connection.close();
                    Display.setMessage("сервер остановлен");
                    server.close();
                }catch (IOException ioe){
                    Display.setMessage("ERROR");
                }
            }
        }
    }

    public static void sendMessageToAllClients(String msg){
        for (ClientHandle o : clients){
            o.sendMsg(msg);
        }
    }
    public void removeClient(ClientHandle client) {
        clients.remove(client);
    }

    public static void Bind(int port) throws IOException {
        ServerSocket server = new ServerSocket();
        server.bind(new InetSocketAddress("127.0.0.1", port));
        server.close();
    }


}
