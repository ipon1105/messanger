import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ClientHandle implements Runnable{
    private Server server;
    private ObjectInputStream input;
    private ObjectOutputStream output;
    private Socket clientSocket;
    private static int clients_count = 0;

    public ClientHandle(Socket socket, Server server){
        clients_count++;
        this.server = server;
        this.clientSocket = socket;
    }

    @Override
    public void run() {
        try {
            while (true) {
                input = new ObjectInputStream(clientSocket.getInputStream());
                Server.sendMessageToAllClients((String) input.readObject());
            }
        }catch (Exception e){

        }
    }

    public void sendMsg(String msg){
        try {
            output = new ObjectOutputStream(clientSocket.getOutputStream());
            output.writeObject((Object) msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void close() {
        sendMsg("Вы были удалены из чата.");

        server.removeClient(this);
    }
}
