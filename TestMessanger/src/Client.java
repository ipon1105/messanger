import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class Client implements Runnable {
    static private ObjectOutputStream output;
    static private ObjectInputStream input;
    static private Socket connection;
    static private Thread thread;
    private static Boolean run = false;

    public void start(){
        if (run) return;
        run = true;

        thread = new Thread(this::run);
        thread.start();
    }

    public void stop(){
        setData("\t"+PersonalData.Name + " вышел(-ла) из чата.");
        if (!run) return;
        run = false;
        try{
            connection.close();
            thread.join(300);
        }catch (Exception e){
            Display.setMessage("Ошибка остановки клиента.");
        }
    }

    @Override
    public void run() {
        try {
            connection = new Socket(InetAddress.getByName(Setting.IP4), Setting.PORT);

            setData("\t"+PersonalData.Name + " вошёл(-ла) в чат.");
        } catch (IOException e) {
            Display.setMessage("Ошибка соединения с "+Setting.IP4+":"+Setting.PORT+";");
            return;
        }

        Thread threadIn = new Thread(this::in);
        threadIn.start();
    }

    public void in(){
        int score = 0;
        while (run){
            try {
                while (run){
                    input = new ObjectInputStream(connection.getInputStream());
                    Display.setMessage((String) input.readObject());
                }
            } catch (IOException e) {
                score++;
                Display.setMessage("Сервер разорвал текущее соединение.");
                try{
                    if (score>=3)stop();
                    Thread.sleep(1000);
                }catch (InterruptedException ie){

                }
            } catch (ClassNotFoundException cl){
                Display.setMessage("Ошибка чтения файла");
                try{
                    Thread.sleep(1000);
                }catch (InterruptedException ie){

                }
            }
        }
    }
    //127.0.0.1:123
    public static void setData(Object obj){
        try{
            output = new ObjectOutputStream(connection.getOutputStream());
            output.flush();
            output.writeObject(obj);
        } catch (Exception e){
            Display.setMessage("Ошибка отправки сообщения");
        }
    }
}
